import 'normalize.css'
import './style.css'
import { Network } from 'vis-network'
const { EventSource, localStorage } = window

const es = new EventSource('/events')
const container = document.getElementById('network')
const networkInitData = {
  nodes: [],
  edges: [],
}
const networkOptions = {
  nodes: {
    borderWidth: 4,
    size: 30,
    color: {
      background: '#2f3136',
    },
    shadow: true,
    font: { color: '#eeeeee' },
  },
  edges: {
    color: {
      color: '#7289da',
      highlight: '#d4415b',
    },
    width: 2,
    shadow: true,
  },
  layout: {
    improvedLayout: false,
  },
  physics: {
    stabilization: {
      iterations: 1000,
    },
  },
}
const cache = localStorage.cache && JSON.parse(localStorage.cache)
if (localStorage.debug) window.es = es
let network

es.onmessage = async e => {
  if (localStorage.debug) console.time('parsed frame')
  const data = JSON.parse(e.data)
  if (localStorage.debug) {
    console.timeEnd('parsed frame')
    console.log(`frame type: ${data.type}`)
  }
  switch (data.type) {
    case 'DATA': {
      if (localStorage.debug) console.log('data frame recieved', data)
      const { users, rels } = data
      for (const user of users) {
        if (!user) continue
        if (!rels.length) {
          const edgeIds = network.body.edgeIndices
          if (!edgeIds.some(id => id.includes(user.id))) continue
        }
        const node = {
          id: user.id,
          shape: 'circularImage',
          image: user.avatar || 'broken',
          brokenImage: 'https://discordapp.com/assets/322c936a8c8be1b803cd94861bdfa868.png',
          label: user.name,
          color: user.color.replace('#000000', '#2f3136'),
          value: (user.value || 0),
          scaling: {
            min: 14,
            max: 36,
          },
        }
        if (network) {
          if (network.body.data.nodes.get(user.id)) {
            network.body.data.nodes.update(node)
          } else {
            network.body.data.nodes.add(node)
          }
        } else {
          let pushed = false
          if (cache) {
            const cachedNode = cache.find(n => n.id === node.id)
            if (cachedNode) {
              const newNode = Object.assign(cachedNode, node)
              networkInitData.nodes.push(newNode)
              pushed = true
            }
          }
          if (!pushed) networkInitData.nodes.push(node)
        }
      }
      for (const rel of rels) {
        if (!rel) continue
        const edge = {
          id: rel.pair.join(':'),
          from: rel.pair[0],
          to: rel.pair[1],
          value: rel.value,
        }
        if (network) {
          if (network.body.data.edges.get(edge.id)) {
            network.body.data.edges.update(edge)
          } else {
            network.body.data.edges.add(edge)
          }
        } else {
          networkInitData.edges.push(edge)
        }
      }
      if (!network) {
        if (cache) {
          console.log(networkInitData.nodes.length, cache.length)
          if (Math.abs(networkInitData.nodes.length - cache.length) <= 8) {
            networkOptions.physics.stabilization.iterations = 200
          }
        }
        networkOptions.physics.stabilization.updateInterval = Math.ceil(networkOptions.physics.stabilization.iterations / 100)
        if (localStorage.debug) {
          console.log('stabilizing network')
          console.time('stabilized network')
        }
        window.network = network = new Network(container, networkInitData, networkOptions)
        network.on('stabilizationProgress', params => {
          if (localStorage.debug) console.log(`stabilization iterations: ${params.iterations}/${params.total}`)
          const ratio = params.iterations / params.total
          const percentage = Math.round(ratio * 100) + '%'

          document.getElementById('loadingProgess').style.width = percentage
          document.querySelector('.loader-info code').innerText = percentage
        })
        network.once('stabilizationIterationsDone', () => {
          document.getElementById('loadingProgess').style.width = '100%'
          document.querySelector('.loader-info code').innerText = '100%'
          document.querySelector('.loader-box').style.opacity = '0'
          if (localStorage.debug) {
            console.timeEnd('stabilized network')
            console.log('caching dataset')
            console.time('cached dataset')
          }
          network.storePositions()
          const nodesDataset = network.body.data.nodes.get()
          localStorage.cache = JSON.stringify(nodesDataset)
          if (localStorage.debug) console.timeEnd('cached dataset')
        })
      }
      break
    }
  }
}
