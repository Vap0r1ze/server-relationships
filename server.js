require('dotenv').config()

const { join } = require('path')
const asyncRedis = require('async-redis')
const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const Discord = require('discord.js')

const ns = process.env.DB_NAMESPACE
const isDryRun = Boolean(+process.env.DRY_RUN)
const db = asyncRedis.createClient()
const app = express()
const client = new Discord.Client()
const connsAlive = []
function writeFrame (conn, frame) {
  conn.write(`data: ${JSON.stringify(frame)}\n\n`)
}

db.on('error', console.error)
app.use(cors())
app.use(express.static(join(__dirname, 'dist')))
app.use(morgan('tiny'))

app.get('/', (req, res) => {
  res.sendFile(join(__dirname, 'dist/index.html'))
})
app.get('/events', async (req, res) => {
  console.log('client connected, sending initial frame')
  console.time('initial frame')
  res.set({
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    Connection: 'keep-alive',
  })
  connsAlive.push(res)
  req.on('close', () => {
    connsAlive.splice(connsAlive.indexOf(res), 1)
  })
  const [users, rels, userValues] = await db.multi()
    .hgetall(`${ns}:users`)
    .hgetall(`${ns}:rels`)
    .zrange(`${ns}:users:values`, 0, -1, 'WITHSCORES')
    .exec()
  if (users && rels) {
    const frameUsers = []
    const frameRels = []
    for (const [userId, user] of Object.entries(users)) {
      if (!Object.keys(rels).some(rel => rel.includes(userId))) continue
      const userData = JSON.parse(user)
      const valueIndex = userValues.indexOf(userId)
      frameUsers.push({
        id: userId,
        ...userData,
        value: valueIndex === -1 ? 0 : +userValues[valueIndex + 1],
      })
    }
    for (const [pairKey, relValue] of Object.entries(rels)) {
      frameRels.push({
        pair: pairKey.split(':'),
        value: +relValue,
      })
    }
    writeFrame(res, { type: 'DATA', users: frameUsers, rels: frameRels })
    writeFrame(res, { type: 'HEARTBEAT' })
  }
  console.timeEnd('initial frame')
})

setInterval(() => {
  for (const conn of connsAlive) {
    writeFrame(conn, { type: 'HEARTBEAT' })
  }
}, 2000)
app.on(`${ns}:frame`, frame => {
  for (const conn of connsAlive) {
    writeFrame(conn, frame)
  }
})
client.on('message', async message => {
  if (message.guild && message.guild.id === process.env.GUILD_ID) {
    if (!message.author || message.author.bot) return
    const authorId = message.author.id
    const authorData = {
      name: message.author.username,
      avatar: message.author.avatar ? `https://cdn.discordapp.com/avatars/${authorId}/${message.author.avatar}.png?size=128` : message.author.defaultAvatarURL,
      color: message.member.displayHexColor,
    }
    const authorDataSaved = await db.hget(`${ns}:users`, authorId)
    let authorSent = false
    if (authorDataSaved && JSON.stringify(authorData) !== authorDataSaved) {
      console.log(`Updating user data ${authorId}:`, authorDataSaved, '->', JSON.stringify(authorData))
      if (!isDryRun) {
        await db.hset(`${ns}:users`, authorId, JSON.stringify(authorData))
      }
      app.emit(`${ns}:frame`, {
        type: 'DATA',
        users: [
          { id: authorId, ...authorData },
        ],
        rels: [],
      })
      authorSent = true
    }
    if (!message.mentions.members) return
    for (const [mentionId, mention] of message.mentions.members.entries()) {
      if (mention.bot || mentionId === authorId) return
      const pair = [authorId, mentionId].sort()
      const mentionData = {
        name: mention.user.username,
        avatar: mention.user.avatar ? `https://cdn.discordapp.com/avatars/${mentionId}/${mention.user.avatar}.png?size=128` : mention.user.defaultAvatarURL,
        color: mention.displayHexColor,
      }
      let mentionValue, relValue
      if (isDryRun) {
        [mentionValue, relValue] = await db.multi()
          .zscore(`${ns}:users:values`, mentionId)
          .hget(`${ns}:rels`, pair.join(':'))
          .exec()
      } else {
        let ttl
        [, mentionValue, relValue, ttl] = await db.multi()
          .hset(`${ns}:users`, mention.id, JSON.stringify(mentionData))
          .zincrby(`${ns}:users:values`, 1, mentionId)
          .hincrby(`${ns}:rels`, pair.join(':'), 1)
          .ttl(`${ns}:rels`)
          .zincrby(`${ns}:all:users:values`, 1, mentionId)
          .hincrby(`${ns}:all:rels`, pair.join(':'), 1)
          .exec()
        if (ttl === -1) {
          const period = +process.env.RESET_PERIOD
          const offset = +process.env.RESET_OFFSET
          if (period) {
            const now = new Date()
            const newTtl = period - (Math.floor(now.getTime() / 1000) - 60 * now.getTimezoneOffset() + offset) % period
            await db.multi()
              .expire(`${ns}:users:values`, newTtl)
              .expire(`${ns}:rels`, newTtl)
              .exec()
          }
        }
        if (!authorSent) {
          await db.hset(`${ns}:users`, authorId, JSON.stringify(authorData))
        }
      }
      app.emit(`${ns}:frame`, {
        type: 'DATA',
        users: [
          !authorSent && { id: authorId, ...authorData },
          { id: mentionId, ...mentionData, value: +mentionValue },
        ],
        rels: [
          { pair, value: relValue },
        ],
      })
    }
  }
})

if (+process.env.DISABLE_COLLECTION) {
  app.listen(process.env.PORT, () => {
    console.log(`app listening to port ${process.env.PORT}`)
  })
} else {
  client.login(process.env.TOKEN).then(() => {
    console.log('client connected')
    app.listen(process.env.PORT, () => {
      console.log(`app listening to port ${process.env.PORT}`)
    })
  }).catch(console.error)
}
